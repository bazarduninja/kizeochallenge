import { KizeoShallPage } from './app.po';

describe('kizeo-shall App', function() {
  let page: KizeoShallPage;

  beforeEach(() => {
    page = new KizeoShallPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
