/**
 * Created by spoinky on 14/02/17.
 */

import {Component, OnInit, Input} from '@angular/core';
import { KizeoService } from '../kizeo.service';

declare var $ : any;

@Component({
    selector: 'data-details',
    templateUrl: 'data-details.component.html'
})
export class DataDetailsComponent implements OnInit
{
    @Input() set id ( id: number ) { this.detailsFetch( id ); }

    results : Promise<String>;
    details = null;
    table = null;

    constructor (private kizeoService: KizeoService) {}

    ngOnInit()
    {
        this.table = $('#dataTable_details').DataTable(
            {
                columns: [
                    { data: 'form_id' },
                    { data: 'id' },
                    { data: 'create_time' },
                    { data: 'answer_time' },
                    { data: 'record_number' },
                    { data: 'user_id' }
                ],
                order: [[ 0, 'asc' ], [ 1, 'asc' ]]
            });
    }

    detailsFetch( id : number )
    {
        this.detailsRequest(id).then( answer => { this.detailsDisplay(answer, true); } ).catch( answer => { this.detailsDisplay(answer, false); } );
    }

    detailsRequest( id : number )
    {
        this.results = this.kizeoService.getDetails(id);
        return this.results;
    }

    detailsDisplay( pAnswer, pShallPass )
    {
        if(pShallPass)
        {
            this.details = pAnswer;
            this.table.clear().rows.add(pAnswer).draw();
        }
    }
}