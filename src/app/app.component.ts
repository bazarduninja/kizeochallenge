import { Component } from '@angular/core';
import { KizeoService } from './kizeo.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ KizeoService ]
})
export class AppComponent
{
  title = 'Kizeo : Challenge';

  constructor (private kizeoService: KizeoService) { console.log(this.kizeoService); }

}
