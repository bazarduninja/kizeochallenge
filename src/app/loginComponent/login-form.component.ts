/**
 * Created by spoinky on 14/02/17.
 */

import { Component } from '@angular/core';
import { KizeoService } from '../kizeo.service';
import { User }    from '../user';

@Component({
    moduleId: module.id,
    selector: 'login-form',
    templateUrl: 'login-form.component.html'
})
export class LoginFormComponent {

    results : Promise<String>;

    constructor (private kizeoService: KizeoService) { }

    corps = ['KIZIO', 'GOOGLE', 'APPLE', 'STAKIZ'];

    model = new User( null, 'login', 'mdp');

    submitted = false;
    checkFail = false;

    onSubmit()
    {
        this.loginRequest().then( answer => { this.isLog(answer, true); } ).catch( answer => { this.isLog(answer, false); } );
    }

    isLog( pAnswer, pShallPass)
    {
        console.log(pAnswer);

        if(pShallPass)
        {
            this.kizeoService.token = pAnswer.token;
            this.submitted = true;
        }
        else { this.checkFail = true; }

        console.log(this.kizeoService);
    }

    loginRequest () {
        this.results = this.kizeoService.getToken(this.model);
        return this.results;
    }
}


