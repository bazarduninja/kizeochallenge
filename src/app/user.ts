export class User {
    constructor(
        public token?   : string,
        public login?   : string,
        public pwd ?    : string,
        public corp?    : string
    ) {  }
}
