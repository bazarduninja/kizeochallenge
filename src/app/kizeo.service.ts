import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { User } from './user';

@Injectable()
export class KizeoService
{
    private apiUrl = 'https://www.kizeoforms.com:443/rest/v3/';  // URL to web API

    public token = null;

    constructor (private http: Http) {}

    getToken (logParam: User): Promise<String>
    {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.apiUrl+'login', {"user": logParam.login, "password": logParam.pwd, "company": logParam.corp}, options)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getForms()
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', this.token);

        let options = new RequestOptions({ headers: headers });
        return this.http.get(this.apiUrl+'forms', options)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getDetails( id : number)
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', this.token);

        let options = new RequestOptions({ headers: headers });
        return this.http.get(this.apiUrl+'forms/'+ id +'/data/all', options)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response)
    {
        let body = res.json();
        return body.data || body.forms ||{ };
    }

    private handleError (error: Response | any)
    {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Promise.reject(errMsg);
    }
}
