/**
 * Created by spoinky on 14/02/17.
 */

import { Component, OnInit } from '@angular/core';
import { KizeoService } from '../kizeo.service';
import {Router, ActivatedRoute, Data} from '@angular/router';

declare var $ : any;

@Component({
    selector: 'data-table',
    templateUrl: 'data-table.component.html'
})
export class DataTableComponent implements OnInit {

    results : Promise<String>;

    constructor (private kizeoService: KizeoService, private router : Router) { this.formsFetch(); }

    forms = null;
    table = null;
    displayId = null;

    ngOnInit()
    {
        this.table = $('#dataTable_forms').DataTable(
            {
                columns: [
                    { data: 'class' },
                    { data: 'name' },
                    { data: 'update_time' },
                    { data: 'id' }
                ],
                order: [[ 0, 'asc' ], [ 1, 'asc' ]]
            });

        var myself = this;

        $('#dataTable_forms').on( 'click', 'tr', function ()
        {
            var rowIdx = myself.table.row( this ).index();
            var colData_id = myself.table.cell( rowIdx, 3 ).data();
            myself.displayId = colData_id;
        });
    }

    formsFetch()
    {
        this.formsRequest().then( answer => { this.displayForms(answer, true); } ).catch( answer => { this.displayForms(answer, false); } );
    }

    formsRequest()
    {
        this.results = this.kizeoService.getForms();
        return this.results;
    }

    displayForms( pAnswer, pShallPass)
    {
        if(pShallPass)
        {
            this.forms = pAnswer;
            this.table.clear().rows.add(pAnswer).draw();
        }
    }
}