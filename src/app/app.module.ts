import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { LoginFormComponent } from './loginComponent/login-form.component';
import { DataTableComponent } from './tablesComponent/data-table.component';
import { DataDetailsComponent } from './detailsComponent/data-details.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginFormComponent },
  { path: 'table', component: DataTableComponent },
  { path: 'details/:id', component: DataDetailsComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    DataTableComponent,
    DataDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }